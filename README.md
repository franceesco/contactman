# Contact Manager

## Requirements 
The application was developed using Python 3.7.0.
To set up an environment create a new virtualenv, activate it, then install the packages using
either requirements.txt or requirement-dev.txt if you wish to create DB migrations and run the tests
as well.

## Configuration
The config file is located next to the contactman package and contains the following configuration options:
* DEBUG: specified whether to Flask whether to run in debug mode 
* LOG_CONFIG: dictionary based log config which specifies the log format, the log level, the logger
  and the handlers
* MAX_CONTACT_AGE: contacts that have been created more then this many seconds are deleted by the
  `create_random_contact` task
* SQLALCHEMY_AUTOFLUSH: flag to control the autoflush behavior of SQLAlchemy
* SQLALCHEMY_DATABASE_OPTIONS: extra options used by SQLAlchemy when creating the DB connection. Currently the
  connection timeout is specified to avoid DB locked errors when the worker and the API concurrently try to access
  the DB.
* SQLALCHEMY_DATABASE_URI: the database URI for SQLAlchemy, currently a Sqlite DB is specified
* beat_schedule: periodic tasks initiated by the scheduler, includes the `create_random_contact` task which is run
  every 15 seconds 
* broker_url: Celery broker url
* celery_log_level: log level for the Celery scheduler and worker
* imports: modules that contain the Celery tasks
* result_backend: Celery result backend
* worker_concurrency: number of the Celery child processes

## Set up the database
The scripts directory contains tools which facilitate the management of the DB:
* db_downgrage.sh `<version>`: downgrade the schema to the given version if all the migrations between the current
  and the desired version are reversible
* db_list_versions.sh: lists the consecutive alembic migration versions
* db_migrate.sh `[-m <name>]` `[--autogenerate]`: creates a new DB migration. The name parameter is included in
  the filename and the description. In autogenerate mode alembic compares the models to the state of the DB
  specified in the config file and generates the changes based on the diff.
* db_upgrade.sh `[version]`: upgrades the schema to the given version. If the version is omitted, then
  the latest is used.

To create a new database (the Sqlite DB is created when a connection is established for the first time) and the tables
or upgrade an existing one run the following script from the project directory:
```bash
scripts/db_upgrade.sh [version]
```

## Migrations
The current migration versions:
* intial: the initial migration for part 1
* separate_email: migration for part 2 which creates a the new `email` table and removes the email column from
  `contact`. It saves the email addresses to separate email records for the contacts before dropping the email column.
* contact_created_at: adds the `created_at` datetime field to the `contact` table. It sets the actual
  timestamp for the existing records.

Since Sqlite doesn't support altering tables and columns one has to create a new table and copy the data, then remove
the old one. To be able to keep child records the foreign keys have to be disabled before the copy.

## Running the application
Execute the run.py module located next to this readme file either from the IDE or from the command
line with the following after activating the virtualenv:
```bash
python run.py
```

## Using the application
The application listens on port 5000 and the index page can be accessed at http://localhost:5000/contacts.
The following endpoints can be accessed using the following paths:
* list contacts: /contacts/ - GET
* get contact by username: /contacts/<contact_id> - GET
* create contact: /contacts/ - POST
* update contact: /contacts/<contact_id> - PUT
* delete contact: /contacts/<contact_id> - DELETE

## Tests
For testing pytest is used, to setup the environment use requirements-dev.txt. To run the tests either use
the IDE or execute the following from the command line in the root directory after activating the virtualenv:
```bash
pytest tests
```

The tests cover the components routes and services.

## Celery
### Scheduler
The scheduler can be run with the following command or from the IDE:
```bash
python scheduler
```

The scheduler uses Celery beat to run the `create_random_contact` task every 15 seconds.

### Worker
The worker can be executed using the command below or using the IDE:
```bash
python worker.py
``` 

The Celery worker is started with a single process to prevent concurrent execution of the same task.
