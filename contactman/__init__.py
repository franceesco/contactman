from celery import Celery
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import event
from sqlalchemy.engine import Engine
from sqlite3 import Connection as SQLite3Connection

from contactman.utils import (
    load_config,
    setup_logging
)


config = load_config()
app = Flask(__name__)
app.config.from_mapping(config)

celery = Celery()
celery.config_from_object(config)

db = SQLAlchemy(app, session_options=dict(autoflush=config['SQLALCHEMY_AUTOFLUSH']))

# Import the models, so that Flask-SQLAlchemy registers them
from contactman import models


def init_app():
    setup_logging(config=config)

    # Register the error handlers
    from contactman import error_handlers

    # Register the routes
    from contactman.routes import contact_api
    app.register_blueprint(contact_api)

    return app


@event.listens_for(Engine, "connect")
def _set_sqlite_pragma(conn, connection_record):
    if isinstance(conn, SQLite3Connection):
        cursor = conn.cursor()
        # By default the foreign keys are disable for Sqlite, so enable them
        cursor.execute("PRAGMA foreign_keys=ON;")
        cursor.close()
