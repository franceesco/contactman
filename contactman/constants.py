class ErrorCodes:
    NotFound = 'NOT_FOUND'
    ValidationError = 'VALIDATION_ERROR'
    Unknown = 'UNKNOWN'
