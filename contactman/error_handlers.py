import logging

from flask import jsonify
from werkzeug.exceptions import HTTPException

from contactman import app
from contactman.constants import ErrorCodes
from contactman.exceptions import ApplicationException


logger = logging.getLogger(__name__)


_ERROR_CODE_MAP = {
    ErrorCodes.NotFound: 404,
    ErrorCodes.Unknown: 500,
    ErrorCodes.ValidationError: 400,
}


@app.errorhandler(ApplicationException)
def handle_application_error(error):
    logger.error('Application error while processing request: {}'.format(str(error)))

    return _create_error_resp(
        code=_ERROR_CODE_MAP.get(error.error_code, 500),
        error_code=error.error_code,
        message=error.details
    )


@app.errorhandler(HTTPException)
def handle_http_error(error):
    logger.exception('HTTP error while processing request: {}'.format(str(error)))

    return _create_error_resp(code=error.code, error_code=ErrorCodes.Unknown, message=error.description)


def _create_error_resp(code, error_code, message):
    resp = {
        'error_code': error_code,
        'message': message,
    }
    return jsonify(resp), code
