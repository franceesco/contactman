from contactman.constants import ErrorCodes


class ApplicationException(Exception):
    error_code = ErrorCodes.Unknown
    details = 'Internal server error occurred.'

    def __init__(self, error_code=None, details=None):
        self.error_code = error_code or self.error_code
        self.details = details or self.details


class NotFoundException(ApplicationException):
    error_code = ErrorCodes.NotFound


class ValidationException(ApplicationException):
    error_code = ErrorCodes.ValidationError
