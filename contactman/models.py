from sqlalchemy import func
from sqlalchemy.orm import relationship

from contactman import db


class ModelBase(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)


class Contact(ModelBase):
    __tablename__ = 'contact'

    username = db.Column(db.String(100), unique=True, nullable=False)
    first_name = db.Column(db.String(255), nullable=False)
    last_name = db.Column(db.String(255), nullable=False)
    created_at = db.Column(db.DateTime, server_default=func.now(), nullable=False)

    emails = relationship('Email', backref='contact', uselist=True)

    @property
    def email_addresses(self):
        return [email.email for email in self.emails]


class Email(ModelBase):
    __tablename__ = 'email'

    email = db.Column(db.String(255), unique=True, nullable=False)
    contact_id = db.Column(
        db.Integer,
        db.ForeignKey('contact.id', name='fk_email_contact_id', ondelete='CASCADE'),
        index=True,
        nullable=False
    )
