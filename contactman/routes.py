from flask import (
    Blueprint,
    jsonify,
    request
)

from contactman.schemas import (
    contact_schema,
    validate_json_request
)
from contactman.services import (
    create_contact,
    delete_contact,
    get_contact_by_username_or_email,
    get_contacts,
    update_contact
)


contact_api = Blueprint('api', __name__, url_prefix='/contacts')


@contact_api.route('/', methods=['GET'])
def contact_list():
    contacts = get_contacts()
    return jsonify(contact_schema.dump(contacts, many=True))


@contact_api.route('/<username_or_email>', methods=['GET'])
def contact_get_by_username_or_email(username_or_email):
    contact = get_contact_by_username_or_email(username_or_email=username_or_email)
    return jsonify(contact_schema.dump(contact))


@contact_api.route('/', methods=['POST'])
@validate_json_request(schema=contact_schema)
def contact_create():
    contact = create_contact(**request.json)
    return jsonify(contact_schema.dump(contact))


@contact_api.route('/<int:contact_id>', methods=['PUT'])
@validate_json_request(schema=contact_schema)
def contact_update(contact_id):
    contact = update_contact(contact_id=contact_id, **request.json)
    return jsonify(contact_schema.dump(contact))


@contact_api.route('/<int:contact_id>', methods=['DELETE'])
def contact_delete(contact_id):
    delete_contact(contact_id=contact_id)
    return jsonify({})
