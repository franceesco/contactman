from functools import wraps

from flask import request
from marshmallow.fields import (
    List,
    String
)
from marshmallow.validate import (
    Length,
    Regexp
)
from marshmallow_sqlalchemy import (
    ModelSchema,
    field_for
)

from contactman.exceptions import ValidationException
from contactman.models import Contact


name_length = Length(max=255)
name_regex = Regexp(r'[^\W\d_]+(( )+[^\W\d_]+)?')


class ContactSchema(ModelSchema):
    class Meta:
        model = Contact
        transient = True
        # The dump_only list specifies fields that are included in serialization but treated as unknown fields when
        # loading or validating
        dump_only = ('id',)
        exclude = ('created_at', 'emails',)

    first_name = field_for(Contact, 'first_name', validate=[name_length, name_regex])
    last_name = field_for(Contact, 'last_name', validate=[name_length, name_regex])
    username = field_for(Contact, 'username', validate=[Length(min=3, max=255), Regexp(r'\A[\w]+([._-]?[\w]+)*\Z')])
    email_addresses = List(
        String(
            # Only allows a subset of the valid email addresses
            validate=[Length(max=255), Regexp(r'\A[\w\-.#]+@[\w]+(.[\w-]+)*.[\w-]{2,6}\Z')]
        ),
        required=True,
        validate=[Length(min=1, max=10)]
    )


def validate_json_request(schema):
    '''Decorator to perform validation of the JSON data using the given marshmallow schema.'''

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            errors = schema.validate(request.json)
            if errors:
                raise ValidationException(details=errors)

            return func(*args, **kwargs)

        return wrapper

    return decorator


contact_schema = ContactSchema()
