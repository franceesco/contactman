"""This module contains the business logic that implements the CRUD operations for the application. The services
are used by the routes.
"""


from functools import partial
import logging
import re

from sqlalchemy import or_
from sqlalchemy.exc import IntegrityError

from contactman import db
from contactman.exceptions import (
    NotFoundException,
    ValidationException
)
from contactman.models import (
    Contact,
    Email
)
from contactman.utils import update_object_attributes


logger = logging.getLogger(__name__)

_UNIQUE_CONSTRAINT_ERROR_PATTERN = re.compile(r'UNIQUE constraint failed: [\w]+\.(?P<field>[\w]+)')


def get_contacts():
    contacts = Contact.query.all()
    return contacts


def get_contact_by_username_or_email(username_or_email):
    contact = _get_contact_by_username_or_email(username_or_email=username_or_email)
    if not contact:
        raise NotFoundException(details="There no contact with username or email '{}'.".format(username_or_email))

    return contact


def create_contact(**kwargs):
    contact = Contact()
    _update_contact(contact=contact, **kwargs)
    db.session.commit()
    return contact


def update_contact(contact_id, **kwargs):
    contact = get_contact_by_id(contact_id=contact_id)
    _update_contact(contact=contact, **kwargs)
    db.session.commit()
    return contact


def delete_contact(contact_id):
    # Check if a contact exists with the given id
    get_contact_by_id(contact_id=contact_id)
    # Cascade delete is used to make sure the emails are removed
    Contact.query.filter(Contact.id == contact_id).delete()
    db.session.commit()


def get_contact_by_id(contact_id):
    contact = Contact.query.filter(Contact.id == contact_id).first()
    if not contact:
        raise NotFoundException(details="There's no contact for id {}.".format(contact_id))

    return contact


def _get_contact_by_username(username):
    contact = (
        Contact.query
        .filter(Contact.username == username)
        .first()
    )
    return contact


def _get_contact_by_username_or_email(username_or_email):
    contact = (
        Contact.query
        .join(Email)
        .filter(or_(Contact.username == username_or_email, Email.email == username_or_email))
        .first()
    )
    return contact


def _get_emails_by_addresses(email_addresses):
    emails = (
        Email.query
        .filter(Email.email.in_(email_addresses))
        .all()
    )
    return emails


def _update_contact(contact, username, email_addresses, **kwargs):
    '''Updates the given contact and attempt to write the changes to the DB. If the unique constraint defined for the
    username or the email field is violated, then return an error.
    '''

    _check_emails_unique(contact=contact, email_addresses=email_addresses)
    _check_username_unique(contact=contact, username=username)
    kwargs.update(dict(username=username))
    contact = _update_object(obj=contact, **kwargs)
    _update_email_addresses(contact=contact, email_addresses=email_addresses)
    return contact


def _check_emails_unique(contact, email_addresses):
    emails = _get_emails_by_addresses(email_addresses=email_addresses)
    taken_address_set = {email.email for email in emails if email.contact_id != contact.id}

    if not taken_address_set:
        return

    field_map = {
        i: email_address
        for i, email_address in enumerate(email_addresses)
        if email_address in taken_address_set
    }
    _raise_field_unique_error(details={'email_addresses': _create_unique_error_details(field_map)})


def _check_username_unique(contact, username):
    if contact.username == username:
        return

    other_contact = _get_contact_by_username(username=username)
    if other_contact and other_contact.id != contact.id:
        _raise_field_unique_error(details=_create_unique_error_details({'username': username}))


def _update_email_addresses(contact, email_addresses):
    specified_email_address_set = set(email_addresses)
    act_email_address_set = set(contact.email_addresses)

    # Create the new emails, the existing ones will be skipped
    for i, email_address in enumerate(email_addresses):
        if email_address not in act_email_address_set:
            email = Email(email=email_address)
            _update_object(
                obj=email,
                unique_error_handler=partial(_handle_email_unique_error, idx=i, email_address=email_address),
                contact_id=contact.id
            )

    old_email_ids = [email.id for email in contact.emails if email.email not in specified_email_address_set]
    # Delete the old emails
    Email.query.filter(Email.id.in_(old_email_ids)).delete(synchronize_session='fetch')


def _handle_email_unique_error(idx, email_address, exc):
    details = {'email_addresses': _create_unique_error_details({idx: email_address})}
    _raise_field_unique_error(details=details)


def _update_object(obj, unique_error_handler=None, **kwargs):
    try:
        db.session.add(obj)
        update_object_attributes(obj=obj, **kwargs)
        db.session.flush()
    except IntegrityError as e:
        logger.exception(str(e))

        match = _UNIQUE_CONSTRAINT_ERROR_PATTERN.search(e.args[0])
        if match:
            field = match.group('field')
            value = getattr(obj, field)
            if unique_error_handler:
                unique_error_handler(exc=e)
            else:
                _raise_field_unique_error(details=_create_unique_error_details({field: value}))
        else:
            raise ValidationException(details='Invalid data.')
    else:
        return obj


def _create_unique_error_details(field_map):
    details = {
        str(field): ["The selected value '{}' is already taken.".format(value)]
        for field, value in field_map.items()
    }
    return details


def _raise_field_unique_error(details):
    raise ValidationException(details=details)
