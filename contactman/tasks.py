from datetime import timedelta
import logging

from sqlalchemy import func

from contactman import (
    celery,
    db
)
from contactman import config
from contactman.models import Contact
from contactman.services import _update_contact
from contactman.utils import generate_random_string


logger = logging.getLogger(__name__)


@celery.task()
def create_random_contact():
    transaction_dt = db.session.query(func.now()).scalar()

    # Create a new contact with 2 emails
    username = generate_random_string(str_len=10)
    email_1 = '{}.1@test.co.uk'.format(username)
    email_2 = '{}.2@test.co.uk'.format(username)
    contact = Contact()

    logger.info('Creating contact {} with emails {} and {}'.format(username, email_1, email_2))

    try:
        # Write the contact with the emails to the DB
        _update_contact(
            contact=contact,
            username=username,
            email_addresses=[email_1, email_2],
            first_name='John', last_name='Test'
        )

        earliest_contact_dt = transaction_dt - timedelta(seconds=config['MAX_CONTACT_KEEP_TIME'])

        logger.info('Removing contacts older than {} s'.format(earliest_contact_dt))

        # Delete old contacts
        old_contacts = (
            Contact.query
            .filter(Contact.created_at < earliest_contact_dt)
        )
        old_contacts.delete(synchronize_session='fetch')

        db.session.commit()
    except Exception as e:
        # Roll back is needed when an error occurs in order to be able to use the session next time
        db.session.rollback()

        logger.exception('Error while creating random contacts: {}'.format(str(e)))
