from logging.config import dictConfig
import os
import random
from pathlib import Path
from string import ascii_letters

import yaml


class Config(object):
    '''Config class which supported the index operator. Mainly needed to be able configure Celery which doesn't support
    mapping or dict objects.
    '''

    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)

    def __iter__(self):
        return iter(self.__dict__.items())

    def __getitem__(self, item):
        return self.__dict__[item]

    def get(self, item, default=None):
        return self.__dict__.get(item, default)


def load_config():
    config_file = os.environ.get('CONFIG')
    # If no explicit file is specified via the CONFIG environment variable, then config file is expected to be next to
    # the contactman package.
    config_file = config_file if config_file else Path(__file__).parent.parent / 'config.yml'
    config_map = load_yaml(filename=config_file)
    return Config(**config_map)


def load_yaml(filename):
    with open(filename) as h:
        return yaml.load(h)


def generate_random_string(str_len):
    return ''.join([random.choice(ascii_letters) for _ in range(str_len)])


def setup_logging(config):
    # Initialize logging
    dictConfig(config.get('LOG_CONFIG', {'version': 1}))


def update_object_attributes(obj, **kwargs):
    for name, value in kwargs.items():
        setattr(obj, name, value)
