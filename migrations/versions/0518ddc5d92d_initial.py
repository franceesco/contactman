"""initial

Revision ID: 0518ddc5d92d
Revises: 
Create Date: 2019-05-27 14:55:48.469461

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0518ddc5d92d'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'contact',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('username', sa.String(length=100), nullable=False),
        sa.Column('email', sa.String(length=255), nullable=False),
        sa.Column('first_name', sa.String(length=255), nullable=False),
        sa.Column('last_name', sa.String(length=255), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('email'),
        sa.UniqueConstraint('username')
    )


def downgrade():
    op.drop_table('contact')
