"""separate_email

Revision ID: d68005fd39eb
Revises: 0518ddc5d92d
Create Date: 2019-05-28 02:00:00.670063

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd68005fd39eb'
down_revision = '0518ddc5d92d'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'email',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('email', sa.String(length=255), nullable=False),
        sa.Column('contact_id', sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.ForeignKeyConstraint(['contact_id'], ['contact.id'], name='fk_email_contact_id', ondelete='CASCADE'),
        sa.UniqueConstraint('email')
    )
    op.create_index(op.f('ix_email_contact_id'), 'email', ['contact_id'], unique=False)

    bind = op.get_bind()
    contact_table = sa.Table('contact', sa.MetaData(bind=bind), autoload=True)
    contacts = [
        dict(contact_id=row['id'], email=row['email'])
        for row in bind.execute(sa.select([contact_table.c.id, contact_table.c.email]))
    ]
    if contacts:
        bind.execute('PRAGMA foreign_keys=OFF;')
        email_table = sa.Table('email', sa.MetaData(bind=bind), autoload=True)
        bind.execute(email_table.insert().values(contacts))

    # Sqlite doesn't support altering columns, so a new table is created wo the column, the data is copied, then the
    # old table is dropped
    with op.batch_alter_table('contact') as batch_op:
        batch_op.drop_column('email')


def downgrade():
    # Raise an error to prevent losing data
    raise Exception("Irreversible migration")

    # op.add_column('contact', sa.Column('email', sa.VARCHAR(length=255), nullable=False))
    # op.drop_index(op.f('ix_email_contact_id'), table_name='email')
    # op.drop_table('email')
