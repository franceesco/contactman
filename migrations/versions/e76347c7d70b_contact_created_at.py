"""contact_created_at

Revision ID: e76347c7d70b
Revises: d68005fd39eb
Create Date: 2019-05-28 14:14:40.744829

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e76347c7d70b'
down_revision = 'd68005fd39eb'
branch_labels = None
depends_on = None


def upgrade():
    # Create a new table
    op.create_table(
        'contact_new',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('username', sa.String(length=100), nullable=False),
        sa.Column('first_name', sa.String(length=255), nullable=False),
        sa.Column('last_name', sa.String(length=255), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False, server_default=sa.func.now()),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('username')
    )

    bind = op.get_bind()
    contact_table = sa.Table('contact', sa.MetaData(bind=bind), autoload=True)
    contacts = [
        dict(row.items())
        for row in bind.execute(
            sa.select([
                contact_table.c.id, contact_table.c.username, contact_table.c.first_name, contact_table.c.last_name
            ])
        )
    ]
    if contacts:
        bind.execute('PRAGMA foreign_keys=OFF;')
        contact_new_table = sa.Table('contact_new', sa.MetaData(bind=bind), autoload=True)
        # Copy the contacts to the new table
        bind.execute(contact_new_table.insert().values(contacts))

    # Drop the old one
    op.drop_table('contact')
    # Rename the new
    op.rename_table('contact_new', 'contact')


def downgrade():
    with op.batch_alter_table('contact') as batch_op:
        batch_op.drop_column('created_at')
