#!/usr/bin/env python
from contactman import init_app


def main():
    app = init_app()
    app.run()


if __name__ == '__main__':
    main()
