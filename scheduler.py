#!/usr/bin/env python
from contactman import (
    celery,
    config
)


if __name__ == '__main__':
    celery.Beat(loglevel=config['celery_log_level']).run()
