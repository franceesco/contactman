#!/usr/bin/env bash

cd "$(dirname "${BASH_SOURCE[0]}")/../migrations"
alembic downgrade ${@:-base}
cd - &>/dev/null
