#!/usr/bin/env bash

cd "$(dirname "${BASH_SOURCE[0]}")/../migrations"
alembic upgrade ${@:-head}
cd - &>/dev/null
