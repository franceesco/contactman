class Dummy(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    @property
    def as_dict(self):
        return dict(self.__dict__)
