import pytest

from contactman import init_app
from contactman.constants import ErrorCodes
from tests.conftest import Dummy


@pytest.fixture(autouse=True, scope='session')
def setup_app():
    yield init_app()


@pytest.fixture(autouse=True, scope='session')
def setup_test_client(setup_app):
    yield setup_app.test_client()


class BaseRouteTest(object):
    @pytest.fixture(autouse=True)
    def setup_base(self, setup_test_client):
        self.client = setup_test_client


class TestContactList(BaseRouteTest):
    @pytest.mark.parametrize(
        'contacts',
        [
            [],
            [
                Dummy(
                    id=1,
                    username='john.smith',
                    email_addresses=['john.test@test.co'],
                    first_name='John',
                    last_name='Smith'
                )
            ],
            [
                Dummy(
                    id=1,
                    username='john.smith',
                    email_addresses=['john.test@test.co'],
                    first_name='John',
                    last_name='Smith'
                ),
                Dummy(
                    id=100,
                    username='joe.doe',
                    email_addresses=['joe.doe@test.co', 'joe.doe.1@test.co'],
                    first_name='Joe',
                    last_name='Doe'
                )
            ],
        ]
    )
    def test(self, mocker, contacts):
        mocker.patch('contactman.routes.get_contacts', return_value=contacts)

        resp = self.client.get('/contacts/')

        assert 200 == resp.status_code
        assert [contact.as_dict for contact in contacts] == resp.json


class TestContactGetByUsernameOrEmail(BaseRouteTest):
    def test(self, mocker):
        contact = Dummy(
            id=1,
            username='john.smith',
            email_addresses=['john.test@test.co', 'john.test.2@test.co'],
            first_name='John',
            last_name='Smith'
        )
        mocker.patch('contactman.routes.get_contact_by_username_or_email', return_value=contact)

        resp = self.client.get('/contacts/john.smith')

        assert 200 == resp.status_code
        assert contact.as_dict == resp.json


class TestContactCreate(BaseRouteTest):
    @pytest.fixture(autouse=True)
    def setup(self, mocker):
        self.mock_create_contact = mocker.patch('contactman.routes.create_contact')

    def test_valid_data(self):
        data = dict(
            username='john.smith',
            email_addresses=['john.test@test.co'],
            first_name='John Joseph',
            last_name='Smith West'
        )
        contact = Dummy(id=123, **data)
        self.mock_create_contact.return_value = contact

        resp = self.client.post('/contacts/', json=data)

        assert 200 == resp.status_code
        assert contact.as_dict == resp.json

    @pytest.mark.parametrize(
        ('data', 'errors'),
        [
            (
                {},
                {
                    'username': ['Missing data for required field.'],
                    'email_addresses': ['Missing data for required field.'],
                    'first_name': ['Missing data for required field.'],
                    'last_name': ['Missing data for required field.'],
                }
            ),
            (
                {
                    'username': 'john',
                    'first_name': 'John',
                    'last_name': 'Test',
                    'email_addresses': []
                },
                {'email_addresses': ['Length must be between 1 and 10.']}
            ),
            (
                {
                    'username': 'john$$abc',
                    'first_name': '123',
                    'last_name': '$$aaa**',
                    'email_addresses': ['test@something.com', 'test2@com', 'test3@com']
                },
                {
                    'username': ['String does not match expected pattern.'],
                    'email_addresses': {
                        '1': ['String does not match expected pattern.'],
                        '2': ['String does not match expected pattern.'],
                    },
                    'first_name': ['String does not match expected pattern.'],
                    'last_name': ['String does not match expected pattern.'],
                }
            ),
            (
                {
                    'username': 'jo',
                    'first_name': 'John',
                    'last_name': 'Test',
                    'email_addresses': ['john.test@test.co.uk']
                },
                {'username': ['Length must be between 3 and 255.']}
            ),
        ]
    )
    def test_invalid_data(self, data, errors):
        resp = self.client.post('/contacts/', json=data)

        assert 400 == resp.status_code
        exp_resp = dict(
            error_code=ErrorCodes.ValidationError,
            message=errors
        )
        assert exp_resp == resp.json


class TestContactUpdate(BaseRouteTest):
    def test_valid_data(self, mocker):
        data = dict(
            username='john.smith',
            email_addresses=['john.test@test.co'],
            first_name='John Joseph',
            last_name='Smith West'
        )
        mock_update_contact = mocker.patch('contactman.routes.update_contact')
        contact = Dummy(id=123, **data)
        mock_update_contact.return_value = contact

        resp = self.client.put('/contacts/123', json=data)

        assert 200 == resp.status_code
        assert contact.as_dict == resp.json
        mock_update_contact.assert_called_once_with(contact_id=123, **data)


class TestContactDelete(BaseRouteTest):
    def test(self, mocker):
        mock_delete_contact = mocker.patch('contactman.routes.delete_contact')

        resp = self.client.delete('/contacts/123')

        assert 200 == resp.status_code
        mock_delete_contact.assert_called_once_with(contact_id=123)
