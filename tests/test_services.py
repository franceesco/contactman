from mock import (
    Mock,
    call
)
import pytest
from sqlalchemy.exc import IntegrityError

from contactman.constants import ErrorCodes
from contactman.exceptions import (
    NotFoundException,
    ValidationException
)
from contactman.services import (
    _check_emails_unique,
    _check_username_unique,
    _create_unique_error_details,
    _get_contact_by_username,
    _get_emails_by_addresses,
    _handle_email_unique_error,
    _raise_field_unique_error,
    _update_contact,
    _update_email_addresses,
    _update_object,
    create_contact,
    delete_contact,
    get_contacts,
    get_contact_by_id,
    get_contact_by_username_or_email,
    update_contact
)


@pytest.fixture()
def mock__create_unique_error_details(mocker):
    yield mocker.patch('contactman.services._create_unique_error_details')


@pytest.fixture()
def mock__raise_field_unique_error(mocker):
    yield mocker.patch('contactman.services._raise_field_unique_error')


@pytest.fixture()
def mock__update_contact(mocker):
    yield mocker.patch('contactman.services._update_contact')


@pytest.fixture()
def mock__update_object(mocker):
    yield mocker.patch('contactman.services._update_object')


@pytest.fixture()
def mock_contact_cls(mocker):
    yield mocker.patch('contactman.services.Contact')


@pytest.fixture()
def mock_contact_id(mock_contact_cls):
    mock_contact_cls.id.__eq__ = lambda inst, oth: 'id == {}'.format(oth)
    yield mock_contact_id


@pytest.fixture()
def mock_contact_username(mock_contact_cls):
    mock_contact_cls.username.__eq__ = lambda inst, oth:  'username == {}'.format(oth)
    yield mock_contact_cls


@pytest.fixture()
def mock_contact_query(mocker):
    yield mocker.patch('contactman.services.Contact.query')


@pytest.fixture()
def mock_db(mocker):
    yield mocker.patch('contactman.services.db')


@pytest.fixture()
def mock_email_cls(mocker):
    yield mocker.patch('contactman.services.Email')


@pytest.fixture()
def mock_email_address(mock_email_cls):
    mock_email_cls.email.__eq__ = lambda inst, oth: 'email == {}'.format(oth)


@pytest.fixture()
def mock_email_query(mocker):
    yield mocker.patch('contactman.services.Email.query')


@pytest.fixture()
def mock_get_contact_by_id(mocker):
    yield mocker.patch('contactman.services.get_contact_by_id')


class TestGetContacts(object):
    @pytest.mark.parametrize(
        'exp_contacts',
        [
            [],
            [{}],
            [{'id': 1, 'username': 'test'}],
            [{'id': 1, 'username': 'user1'}, {'email': 'user@mail.com'}],
        ]
    )
    def test(self, mock_contact_query, exp_contacts):
        mock_contact_query.all.return_value = exp_contacts

        contacts = get_contacts()

        assert exp_contacts == contacts


class TestGetContactByUsernameOrEmail(object):
    @pytest.fixture(autouse=True)
    def setup(self, mocker):
        self.mock__get_contact_by_username_or_email = mocker.patch(
            'contactman.services._get_contact_by_username_or_email'
        )

    def test_contact_exists(self):
        exp_contact = Mock(email='dummy@test.com')
        self.mock__get_contact_by_username_or_email.return_value = exp_contact

        contact = get_contact_by_username_or_email(username_or_email='dummy')

        assert exp_contact == contact
        self.mock__get_contact_by_username_or_email.assert_called_once_with(username_or_email='dummy')

    def test_contact_non_existent(self):
        self.mock__get_contact_by_username_or_email.return_value = None

        with pytest.raises(NotFoundException) as exc_info:
            get_contact_by_username_or_email(username_or_email='dummy')

        assert ErrorCodes.NotFound == exc_info.value.error_code
        self.mock__get_contact_by_username_or_email.assert_called_once_with(username_or_email='dummy')


class TestPrivateGetContactByUsernameOrEmail(object):
    def test(self, mocker, mock_contact_username, mock_email_address, mock_contact_query):
        exp_contact = Mock()
        mock_sqlalchemy_or = mocker.patch('contactman.services.or_')
        mock_sqlalchemy_or.side_effect = lambda *a: ' or '.join(a)
        mock_filter = mock_contact_query.join.return_value.filter
        mock_filter.return_value.first.return_value = exp_contact

        contact = get_contact_by_username_or_email(username_or_email='dummy')

        assert exp_contact == contact
        mock_filter.assert_called_once_with(('username == dummy or email == dummy'))


class TestCreateContact(object):
    def test(self, mock_db, mock_contact_cls, mock__update_contact):
        exp_contact = Mock()
        mock_contact_cls.return_value = exp_contact
        kwargs = dict(first_name='John', last_name='Test', nick_name='Johny')

        contact = create_contact(**kwargs)

        assert exp_contact == contact
        mock__update_contact.assert_called_once_with(contact=contact, **kwargs)
        mock_db.session.commit.assert_called_once()


class TestUpdateContact(object):
    def test(self, mock_db, mock__update_contact, mock_get_contact_by_id):
        exp_contact = Mock()
        mock_get_contact_by_id.return_value = exp_contact
        kwargs = dict(email='joe@test.co', title='Mr.')

        contact = update_contact(contact_id=123, **kwargs)

        assert exp_contact == contact
        mock_get_contact_by_id.assert_called_once_with(contact_id=123)
        mock__update_contact.assert_called_once_with(contact=contact, **kwargs)
        mock_db.session.commit.assert_called_once()


class TestDeleteContact(object):
    def test(self, mock_db, mock_contact_id, mock__update_contact, mock_contact_query, mock_get_contact_by_id):
        mock_get_contact_by_id.return_value = Mock()

        result = delete_contact(contact_id=666)

        assert result is None
        mock_get_contact_by_id.assert_called_once_with(contact_id=666)
        mock_contact_query.filter.assert_called_once_with('id == 666')
        mock_contact_query.filter.return_value.delete.assert_called_once()
        mock_db.session.commit.assert_called_once()


class TestGetContactById(object):
    def test_contact_exists(self, mock_contact_id, mock_contact_query):
        exp_contact = Mock()
        mock_contact_query.filter.return_value.first.return_value = exp_contact

        contact = get_contact_by_id(contact_id=123)

        assert exp_contact == contact
        mock_contact_query.filter.assert_called_once_with('id == 123')

    def test_contact_non_existent(self, mock_contact_id, mock_contact_query):
        mock_contact_query.filter.return_value.first.return_value = None

        with pytest.raises(NotFoundException) as exc_info:
            get_contact_by_id(contact_id=456)

        assert ErrorCodes.NotFound == exc_info.value.error_code
        mock_contact_query.filter.assert_called_once_with('id == 456')


class TestPrivateGetEmailsByAddresses(object):
    @pytest.mark.parametrize('exp_emails', [[], [Mock()]])
    def test(self, mock_email_cls, mock_email_query, exp_emails):
        mock_email_query.filter.return_value.all.return_value = exp_emails

        emails = _get_emails_by_addresses(email_addresses=['dummy@test.co'])

        assert exp_emails == emails
        mock_email_cls.email.in_.assert_called_once_with(['dummy@test.co'])


class TestPrivateGetContactByUsername(object):
    @pytest.mark.parametrize('exp_contact', [None, Mock()])
    def test(self, mock_contact_username, mock_contact_query, exp_contact):
        mock_contact_query.filter.return_value.first.return_value = exp_contact

        contact = _get_contact_by_username(username='john.test')

        assert exp_contact == contact
        mock_contact_query.filter.assert_called_once_with('username == john.test')


class TestPrivateUpdateContact(object):
    def test(self, mocker, mock__update_object):
        contact = Mock()
        mock__update_object.return_value = contact
        mock__check_emails_unique = mocker.patch('contactman.services._check_emails_unique')
        mock__check_username_unique = mocker.patch('contactman.services._check_username_unique')
        mock__update_email_addresses = mocker.patch('contactman.services._update_email_addresses')

        result = _update_contact(
            contact=contact, email_addresses=['dummy@test.co'], username='john.test', first_name='John'
        )

        assert result is contact
        mock__check_emails_unique.assert_called_once_with(contact=contact, email_addresses=['dummy@test.co'])
        mock__check_username_unique.assert_called_once_with(contact=contact, username='john.test')
        mock__update_object.assert_called_once_with(obj=contact, username='john.test', first_name='John')
        mock__update_email_addresses.assert_called_once_with(contact=contact, email_addresses=['dummy@test.co'])


class TestPrivateUpdateObject(object):
    @pytest.fixture(autouse=True)
    def setup(self, mocker, mock_db, mock__create_unique_error_details, mock__raise_field_unique_error):
        self.mock_db = mock_db
        self.mock__create_unique_error_details = mock__create_unique_error_details
        self.error_details = Mock()
        self.mock__create_unique_error_details.return_value = self.error_details
        self.mock__raise_field_unique_error = mock__raise_field_unique_error
        self.mock__raise_field_unique_error.side_effect=ValidationException(details=self.error_details)
        self.mock_update_object_attributes = mocker.patch('contactman.services.update_object_attributes')
        self.obj = Mock()
        self.kwargs = dict(
            username='john.test',
            first_name='John'
        )

    def test_success(self):
        obj = _update_object(obj=self.obj, **self.kwargs)

        assert obj is self.obj
        self.mock_db.session.add.assert_called_once_with(self.obj)
        self.mock_update_object_attributes.assert_called_once_with(obj=obj, **self.kwargs)

    def test_field_non_unique_wo_error_handler(self):
        self.mock_db.session.flush.side_effect = IntegrityError(
            # message
            '',
            # statement
            '',
            # The 1. element of the params argument has to contain the message
            ('Unfortunately the UNIQUE constraint failed: contact.username etc!',),
            # original exception
            Exception()
        )

        with pytest.raises(ValidationException):
            _update_object(obj=self.obj, **self.kwargs)

        self.mock_update_object_attributes.assert_called_once_with(obj=self.obj, **self.kwargs)
        self.mock__raise_field_unique_error.assert_called_once_with(details=self.error_details)

    def test_field_non_unique_with_error_handler(self):
        integrity_error = IntegrityError(
            # message
            '',
            # statement
            '',
            # The 1. element of the params argument has to contain the message
            ('Unfortunately the UNIQUE constraint failed: contact.username etc!',),
            # original exception
            Exception()
        )
        self.mock_db.session.flush.side_effect = integrity_error
        mock_error_handler = Mock()

        _update_object(obj=self.obj, unique_error_handler=mock_error_handler, **self.kwargs)

        self.mock_update_object_attributes.assert_called_once_with(obj=self.obj, **self.kwargs)
        mock_error_handler.assert_called_once_with(exc=integrity_error)
        self.mock__raise_field_unique_error.assert_not_called()

    def test_other_integrity_error(self):
        self.mock_db.session.flush.side_effect = IntegrityError(
            # message
            '',
            # statement
            '',
            # The 1. element of the params argument has to contain the message
            ('Other integrity error',),
            # original exception
            Exception()
        )

        with pytest.raises(ValidationException) as exc_info:
            _update_object(obj=self.obj, **self.kwargs)

        assert 'Invalid data.' == exc_info.value.details
        self.mock_update_object_attributes.assert_called_once_with(obj=self.obj, **self.kwargs)

    def test_other_error(self):
        self.mock_db.session.flush.side_effect = Exception('Error')

        with pytest.raises(Exception):
            _update_object(obj=self.obj, **self.kwargs)

        self.mock_update_object_attributes.assert_called_once_with(obj=self.obj, **self.kwargs)


class TestPrivateCheckEmailsUnique(object):
    @pytest.fixture(autouse=True)
    def setup(self, mocker):
        self.contact = Mock(id=123, email='dummy@test.co')
        self.mock__get_emails_by_addresses = mocker.patch('contactman.services._get_emails_by_addresses')

    @pytest.mark.parametrize(
        'email_addresses',
        [
            [],
            ['dummy1@test.co'],
            ['dummy1@test.co', 'dummy2@test.co'],
        ],
    )
    def test_emails_unchanged(self, email_addresses):
        self.mock__get_emails_by_addresses.return_value = [
            Mock(email=email, contact_id=self.contact.id) for email in email_addresses
        ]

        _check_emails_unique(contact=self.contact, email_addresses=['dummy@test.co'])

    @pytest.mark.parametrize(
        'email_addresses',
        [
            [],
            ['dummy1@test.co'],
            ['dummy1@test.co', 'dummy2@test.co'],
        ],
    )
    def test_emails_not_in_use(self, email_addresses):
        self.mock__get_emails_by_addresses.return_value = []

        _check_emails_unique(contact=self.contact, email_addresses=email_addresses)

    @pytest.mark.parametrize(
        ('taken_email_addresses', 'exp_error_details'),
        [
            (['john@test.co'], {'1': ["The selected value 'john@test.co' is already taken."]}),
            (
                ['dummy1@test.co', 'dummy2@test.co'],
                {
                    '1': ["The selected value 'dummy1@test.co' is already taken."],
                    '2': ["The selected value 'dummy2@test.co' is already taken."],
                }
            ),
        ]
    )
    def test_emails_taken(self, taken_email_addresses, exp_error_details):
        unused_email_addresses = ['unused@test.co']
        act_email_addresses = ['non_existent@test.co']
        existing_email_addresses = (
            [Mock(email=email_address, contact_id=666) for email_address in taken_email_addresses] +
            [Mock(email=email_address, contact_id=self.contact.id) for email_address in act_email_addresses]
        )
        self.mock__get_emails_by_addresses.return_value = existing_email_addresses

        with pytest.raises(ValidationException) as exc_info:
            _check_emails_unique(
                contact=self.contact,
                email_addresses=unused_email_addresses + taken_email_addresses + act_email_addresses
            )

        assert ErrorCodes.ValidationError == exc_info.value.error_code
        assert {'email_addresses': exp_error_details} == exc_info.value.details


class TestPrivateCheckUsernameUnique(object):
    @pytest.fixture(autouse=True)
    def setup(self, mocker):
        self.contact = Mock(username='john.test')
        self.mock__get_contact_by_username = mocker.patch('contactman.services._get_contact_by_username')

    def test_username_unchanged(self):
        _check_username_unique(contact=self.contact, username='john.test')

        self.mock__get_contact_by_username.assert_not_called()

    def test_no_contact_for_username(self):
        self.mock__get_contact_by_username.return_value = None

        _check_username_unique(contact=self.contact, username='joe.test')

        self.mock__get_contact_by_username.assert_called_once_with(username='joe.test')

    def test_contact_for_username_is_same(self):
        self.mock__get_contact_by_username.return_value = self.contact

        _check_username_unique(contact=self.contact, username='joe.test')

        self.mock__get_contact_by_username.assert_called_once_with(username='joe.test')

    def test_username_is_taken(self):
        with pytest.raises(ValidationException) as exc_info:
            _check_username_unique(contact=self.contact, username='joe.test')

        assert ErrorCodes.ValidationError == exc_info.value.error_code


class TestPrivateUpdateEmailAddresses(object):
    @pytest.mark.parametrize(
        ('act_email_addresses', 'email_addresses', 'new_email_addresses', 'deleted_email_addresses'),
        [
            ([], [], [], []),
            (['act@test.co'], ['new@test.co'], ['new@test.co'], []),
            (['act@test.co'], ['new@test.co', 'act@test.co'], ['new@test.co'], []),
            (['act@test.co', 'old@test.co'], ['act@test.co'], [], ['old@test.co']),
            (['act@test.co', 'old@test.co'], ['act@test.co', 'new@test.co'], ['new@test.co'], ['old@test.co']),
        ]
    )
    def test(
            self, mocker, mock_db, mock_email_cls, mock_email_query, mock__update_object, act_email_addresses,
            email_addresses, new_email_addresses, deleted_email_addresses):
        mock_partial = mocker.patch('contactman.services.partial')
        contact = Mock(email_addresses=email_addresses)
        contact.emails = [Mock(email=email_addr) for email_addr in act_email_addresses]
        contact.email_addresses = act_email_addresses

        _update_email_addresses(contact=contact, email_addresses=email_addresses)

        exp__update_object_calls = [
            call(
                obj=mock_email_cls.return_value,
                unique_error_handler=mock_partial.return_value,
                contact_id=contact.id
            )
            for _ in new_email_addresses
        ]
        assert exp__update_object_calls == mock__update_object.mock_calls
        exp_partial_calls = [
            call(_handle_email_unique_error, idx=email_addresses.index(email_addr), email_address=email_addr)
            for email_addr in new_email_addresses
        ]
        assert mock_partial.mock_calls == exp_partial_calls
        mock_db.session.add.mock_calls = [call(mock_email_cls.return_value)]
        mock_email_query.filter.assert_called_once_with(mock_email_cls.id.in_())
        mock_email_query.filter.return_value.delete.assert_called_once()


class TestPrivateHandleEmailUniqueError(object):
    def test(self, mock__create_unique_error_details, mock__raise_field_unique_error):
        mock__create_unique_error_details.return_value = {'123': 'Error'}

        _handle_email_unique_error(idx=123, email_address='dumy', exc=Mock())

        mock__raise_field_unique_error.assert_called_once_with(details={'email_addresses': {'123': 'Error'}})


class TestPrivateCreateErrorDetails(object):
    def test(self):
        details = _create_unique_error_details({'field1': 'value1', 'field2': 'value2'})

        exp_details = {
            'field1': ["The selected value 'value1' is already taken."],
            'field2': ["The selected value 'value2' is already taken."],
        }
        assert details == exp_details


class TestPrivateRaiseFieldNonUniqueError(object):
    def test(self):
        with pytest.raises(ValidationException) as exc_info:
            _raise_field_unique_error({'abc': 'XYZ'})

        assert ErrorCodes.ValidationError == exc_info.value.error_code
        assert {'abc': 'XYZ'} == exc_info.value.details
