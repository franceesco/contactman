import pytest

from contactman.utils import update_object_attributes
from tests.conftest import Dummy


class TestUpdateObjectAttributes(object):
    @pytest.mark.parametrize(
        ('existing_attr_map', 'new_attr_map', 'exp_attr_map'),
        [
            ({}, {}, {}),
            ({'first': 'abc'}, {}, {'first': 'abc'}),
            ({}, {'first': 'abc'}, {'first': 'abc'}),
            ({'first': 1}, {'second': 2}, {'first': 1, 'second': 2}),
            ({'first': 1, 'third': 3}, {'first': 10, 'second': 20}, {'first': 10, 'second': 20, 'third': 3}),
        ]
    )
    def test(self, existing_attr_map, new_attr_map, exp_attr_map):
        obj = Dummy(**existing_attr_map)

        update_object_attributes(obj=obj, **new_attr_map)

        assert exp_attr_map == obj.as_dict
